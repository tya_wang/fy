import React from 'react';
import http from '../http/axios';
import ReactPlayer from 'react-player';
import { Upload, Button, message, Table, Typography, Tooltip } from 'antd';
import { AimOutlined, ClearOutlined } from '@ant-design/icons';
import List from './List';
import { Charts, TrajactoryCharts } from './Chart';
import '../style/Home.css';
import homeAnalyze from '../assets/home_analyse.png';
import homeVideo from '../assets/home_viedo.png';
import homeErr from '../assets/home_err.png';

const { Title } = Typography;

function isNumeric(str) {
  if (typeof str != "string") return false
  return !isNaN(str) && 
         !isNaN(parseFloat(str))
}

function isIntRange(str, min, max) {
  if(!isNumeric(str)) {
    return false;
  }
  let res = true;
  let n = Math.floor(Number(str));
  res = res && n !== Infinity
        && String(n) === str;
  if (min != null) {
    res = res && n >= min;
  }
  if (max != null) {
    res = res && n <= max;
  }
  return res;
}

const columns = [{
  title: '人物序号',
  dataIndex: 'id',
  key: 'id',
}]

const field_info = {
  yolo_params: {
    yolo_img_size: {
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v, null, null);
        },
      ]
    },
    vibe_batch_size: {
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v, null, null);
        },
      ]
    },
    MIN_NUM_FRAMES: {
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v, null, null);
        },
      ]
    },
    bbox_scale: {
      description: "描述",
      validations: [
        (v) => {
          return isNumeric(v);
        },
      ]
    },
  },
  openpose_params: {
    net_resolution:{
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v[0], -1, null) && isIntRange(v[1], 0, null);
        },
      ]
    },
    scale_number: {
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v, null, null);
        },
      ]
    },
    scale_gap: {
      description: "描述",
      validations: [
        (v) => {
          return isNumeric(v) && v >= 0 && v <= 1;
        },
      ]
    },
    hand_scale_number: {
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v, null, null);
        },
      ]
    },
    hand_scale_range: {
      description: "描述",
      validations: [
        (v) => {
          return isNumeric(v) && v >= 0 && v <= 1;
        },
      ]
    },
    num_people_max: {
      description: "描述",
      validations: [
        (v) => {
          return isIntRange(v, 1, 10);
        },
      ]
    },
  },
  vibe_params: {
    run_simplify: {
      description: "描述",
      validations: [
        (v) => {
          return ['0', '1'].includes(v);
        },
      ]
    },
    side_view: {
      description: "描述",
      validations: [
        (v) => {
          return ['0', '1'].includes(v);
        },
      ]
    },
    smooth_min_cutoff: {
      description: "描述",
      validations: [
        (v) => {
          return isNumeric(v);
        },
      ]
    },
    smooth_beta: {
      description: "描述",
      validations: [
        (v) => {
          return isNumeric(v);
        },
      ]
    },
  },
}

class Home extends React.Component {
  constructor(props) {
    let params;
    super(props);

    if (!window.localStorage.getItem('params')) {
      let yolo_params, openpose_params, vibe_params;
      yolo_params = {
        yolo_img_size: '416',
        vibe_batch_size: '450',
        MIN_NUM_FRAMES: '25',
        bbox_scale: '1.1',
      };
      openpose_params = {
        net_resolution: ['-1', '256'],
        scale_number: '4',
        scale_gap: '0.25',
        hand_scale_number: '6',
        hand_scale_range: '0.4',
        num_people_max: '1',
      };
      vibe_params = {
        run_simplify: '1',
        side_view: '1',
        smooth_min_cutoff: '0.004',
        smooth_beta: '0.7',
      };
      window.localStorage.setItem('params', JSON.stringify({
        yolo_params: yolo_params,
        openpose_params: openpose_params,
        vibe_params: vibe_params,
      }))
    }

    params = JSON.parse(window.localStorage.getItem('params'));

    this.state = {
      hasHistory: !!(window.localStorage.getItem('fileName')),
      yolo_params: params.yolo_params,
      openpose_params: params.openpose_params,
      vibe_params: params.vibe_params,
      hasUploaded: false,
      isPkl: false,
      videoPerspective: 2,
      allChoices: [],
      selectedChoices: [],
      chartData: [],
      trajectoryData:[],
      loading: true,
      videoPath: '',
      videoPath2d: '',
      videoPath3d: '',
      trackingPath: '',
      videosPlaying: [true, true],
      playingEnded: 0,
    }

    this.generateParamForm = this.generateParamForm.bind(this);
    this.validateParams = this.validateParams.bind(this);
    this.onSelectChange = this.onSelectChange.bind(this);
    this.onSubmitPersonChoice = this.onSubmitPersonChoice.bind(this);
    this.changePerspective = this.changePerspective.bind(this);
    this.getChoices = this.getChoices.bind(this);
    this.getList = this.getList.bind(this);
    this.onUpload = this.onUpload.bind(this);
    this.onPlayingEnded = this.onPlayingEnded.bind(this);
    
    this.originalVideoRef = React.createRef();
    this.perspectiveVideoRef = React.createRef();
  }

  componentDidMount() {
    if (!!(window.localStorage.getItem('fileName'))) {
      this.getList();
    }
  }

  generateParamForm(param_name) {
    return Object.keys(field_info[param_name]).map((field) => {
      return <p key={field}>
        <Tooltip title={field_info[param_name][field].description}>
          <label htmlFor={field}>{field}</label>
        </Tooltip>
        <span>: </span>
        <input
          className='param-form-input'
          name={field}
          value={this.state[param_name][field]}
          onChange={(e) => {
            let new_state = new Object();
            let new_param = this.state[param_name];
            new_param[field] = e.target.value;
            new_state[param_name] = new_param;
            this.setState(new_state, () => {
              window.localStorage.setItem('params', JSON.stringify({
                yolo_params: this.state.yolo_params,
                openpose_params: this.state.openpose_params,
                vibe_params: this.state.vibe_params,
              }))
            });
          }}
        />
      </p>
    })
  }

  validateParams(param_name) {
    let info = field_info[param_name];
    let errors = 0;
    let error_string = 'The following fields have wrong types: ';
    Object.keys(this.state[param_name]).forEach(key => {
      info[key].validations.forEach(f => {
        let res = f(this.state[param_name][key]);
        if (!res) {
          errors += 1;
          error_string = error_string + key + ', ';
        }
      })
    })
    if (errors != 0) {
      message.error(error_string);
      return false;
    }
    return true;
  }

  getList() {
    const t = this;
    http.post('/return_video', {
      input_video_path: window.localStorage.getItem('fileName'),
      select_person: t.state.selectedChoices,
      openpose: t.state.openpose_params,
      vibe: t.state.vibe_params, 
    }).then((res) => {
      this.setState({
        hasHistory: true,
        hasUploaded: false,
        chartData: res.data.chart_data,
        trajectoryData: res.data.trajectory_pos,
        videoPath: res.data.input_video_path,
        videoPath2d: res.data.output_video_path_2d,
        videoPath3d: res.data.output_video_path_3d,
      })
    })
  }

  getChoices() {
    const t = this;
    http.post('/return_frame', {
      input_video_path: window.localStorage.getItem('fileName'),
      yolo: t.state.yolo_params,
    }).then((res) => {
      res.data.nump_person.forEach((p) => {
        p.key = p.id;
      })
      t.setState({
        hasUploaded: true,
        hasHistory: false,
        allChoices: res.data,
        selectedChoices: [],
        loading: false,
        trackingPath: res.data.first_frame,
      })
    })
  }

  onSelectChange(selected) {
    this.setState({
      selectedChoices: selected,
    })
  }

  onSubmitPersonChoice() {
    let validation;
    if (this.state.selectedChoices.length == 0) {
      message.error('请选择至少一个数据');
      return;
    }
    validation = this.validateParams('openpose_params') && this.validateParams('vibe_params');
    if (validation) {
      this.getList();
    }
  }

  onUpload(res) {
    const t = this;
    if (res.file.status === 'done') {
      message.success(`${res.file.name} 上传成功！`);
      window.localStorage.setItem('fileName', res.file.response.url);
      if (res.file.response.url.slice(-3) == 'mp4') {
        t.getChoices();
      } else if (res.file.response.url.slice(-3) == 'pkl') {
        t.getList();
      }
    } else if (res.file.status === 'error') {
      message.error('上传失败，请重试！')
    }
  }

  changePerspective(p) {
    this.setState({
      videoPerspective: p,
    }, () => {
      this.originalVideoRef.current.seekTo(0, 'seconds');
      this.perspectiveVideoRef.current.seekTo(0, 'seconds')
    })
  }

  onPlayingEnded(i) {
    let playing = this.state.videosPlaying;
    playing[i] = false;

    this.setState((state, props) => ({
      videosPlaying: playing,
      playingEnded: state.playingEnded + 1,
    }), () => {
      if (this.state.playingEnded == 2) {
        this.perspectiveVideoRef.current.seekTo(0);
        this.originalVideoRef.current.seekTo(0);
        this.setState({
          videosPlaying: [true, true],
          playingEnded: 0,
        })
      }
    })
  }

  render() {
    return (<>
      <div className='video'>
        <div className='region box justify-between align-center'>
          <div className='region box align-center'>
            <img className='region_icon' src={homeVideo} />
            <div className='region_title'>
              检测结果显示
            </div>
          </div>
          {this.state.hasHistory && <div className='buttons box'>
            <div
              className={this.state.videoPerspective == 2 ? 'button activeBtn' : 'button'}
              style={{ 'marginRight': '20px' }}
              onClick={() => this.changePerspective(2)}>
              二维视图
            </div>
            <div
              className={this.state.videoPerspective == 3 ? 'button activeBtn' : 'button'}
              style={{ 'marginRight': '20px' }}
              onClick={() => this.changePerspective(3)}>
              三维视图
            </div>
          </div>}
          {this.state.hasUploaded ? <Button type='primary' onClick={this.onSubmitPersonChoice}>
            <AimOutlined />
            确定
          </Button> : <div>
            {this.state.hasHistory && <div style={{ display: 'flex' }}>
              <Upload
                action={apiRoot + '/upload'}
                beforeUpload={(res) => this.validateParams('yolo_params', res)}
                onChange={this.onUpload}
                name='file'
              >
                <Button type='primary'>
                  <AimOutlined />
                  上传文件
                </Button>
              </Upload>
              {/* <Button type='default'
                icon={<ClearOutlined />}
                style={{
                  position: 'relative',
                  right: '260px',
                }}
                onClick={() => {
                  window.localStorage.removeItem('fileName');
                  location.reload();
                }} >
                清除历史
              </Button> */}
            </div>}
          </div>}
        </div>
        {this.state.hasHistory && <div className='box justify-between'>
          <ReactPlayer
            url={this.state.videoPath}
            width='90%'
            height='90%'
            muted={true}
            playing={this.state.videosPlaying[0]}
            style={{ marginRight: '1.5%' }}
            // ref={(p) => this.originalVideoRef = p}
            ref={this.originalVideoRef}
            onPlay={() => {
              this.setState({
                videosPlaying: [true, true],
              })
            }}
            onPause={() => {
              this.setState({
                videosPlaying: [false, false],
              })
            }}
            onEnded={() => this.onPlayingEnded(0)}
            controls={true}
          />
          <ReactPlayer
            url={this.state.videoPerspective == 2
              ? this.state.videoPath2d
              : this.state.videoPath3d}
            width='90%'
            height='90%'
            muted={true}
            playing={this.state.videosPlaying[1]}
            style={{ marginLeft: '1.5%' }}
            ref={this.perspectiveVideoRef}
            onEnded={() => this.onPlayingEnded(1)}
          />
        </div>}
        {(!this.state.hasHistory) && (!this.state.hasUploaded) && <div className='box justify-between' style={{
          'width': '100%',
          'height': '100%',
          'paddingBottom': '15%'
        }}>
          <div className="home-title">
            <Title>基于仿真人体的动作还原系统</Title>
            <div>
              <div className="title-buttons">
                <Upload
                  action={apiRoot + '/upload'}
                  beforeUpload={() => this.validateParams('yolo_params')}
                  onChange={this.onUpload}
                  // onChange={(res) => this.validateParams('yolo_params', res)}
                  name='file'
                >
                  <Button
                    type='primary'
                    shape='round'
                    size='large'
                  >
                    <AimOutlined />
                    上传文件
                  </Button>
                </Upload>
              </div>
              <div>
                <form className='param-form' style={{
                    margin: 'auto',
                  }}>
                  {this.generateParamForm('yolo_params')}
                </form>
              </div>
            </div>
          </div>
        </div>}
        {(!this.state.hasHistory) && this.state.hasUploaded && <div className='box justify-between'>
          <div className='viedos'>
            <ReactPlayer
              url={this.state.trackingPath}
              width='100%'
              height='100%'
              muted={true}
              playing={true}
              loop={true}
              controls={true}
            />
          </div>
          <div>
            <div className='select-panel'>
              <Table
                loading={this.state.loading}
                className='chooseTable'
                columns={columns}
                dataSource={this.state.allChoices.nump_person}
                pagination={false}
                scroll={{'y': '400'}}
                rowSelection={{
                  type: 'checkbox',
                  onChange: this.onSelectChange,
                }}/>
            </div>
          </div>
          <div className='bg-white'>
            <div className='select-panel'>
              <form className='param-form' style={{
                margin: 'auto',
              }}>
                <p><span className='form-header'>Openpose Parameters</span></p>
                {this.generateParamForm('openpose_params')}
                <p><span className='form-header'>Vibe Parameters</span></p>
                {this.generateParamForm('vibe_params')}
              </form>
            </div>
          </div>
        </div>}
      </div>
      
      {this.state.chartData.length > 0 &&
      <div>
        <div className="List">
          <div className="region box justify-between">
            <div className="region box align-center">
              <img className="region_icon" src={homeErr} />
              <div className="region_title">
                仿真结果
              </div>
            </div>
          </div>

          <div className="box">
            <List />
          </div>
        </div>

        {/* <div className='List'>
          <div className="region box justify-between">
            <div className="region box align-center">
              <img className="region_icon" src={homeErr} />
              <div className="region_title">
                图表标题
              </div>
            </div>
          </div>
          <div className='box'>
            <TrajactoryCharts data={this.state.trajectoryData} />
          </div>
        </div> */}

        <div className="charts">
          <div className="region box justify-between">
            <div className="region box align-center">
              <img className="region_icon" src={homeAnalyze} />
              <div className="region_title">
                力矩分析
              </div>
            </div>
          </div>

          <Charts data={this.state.chartData} />
        </div>
      </div>}
    </>);
  }
}

export default Home;
