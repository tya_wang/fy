import React from 'react';
import {
  Routes,
  Route,
  Link,
} from "react-router-dom";
import { Layout, Menu } from 'antd';
import About from './About';
import Home from './Home';
import List from './List';
import 'antd/dist/antd.css';
import '../style/App.css';

const { Header, Content } = Layout;

class App extends React.Component {

  nav = [
    {
      name: '系统介绍',
      url: '/about',
    },										
    {
      name: '图像视频仿真',
      url: '/home',
    },
    {
      name: '历史仿真结果',
      url: '/list',
    },
  ]

  constructor(props) {
    super(props);
    this.state = {
      activeUrl: window.location.pathname,
    }
    this.onChangeView = this.onChangeView.bind(this);
  }

  onChangeView(e) {
    this.setState({
      activeUrl: e.key,
    })
  }

  render() { 
    return (<Layout className='app'>
      <Header
        style={{
          'height': '62px',
          'borderBottom': '1px solid #2D468B',
          'padding': '0 35px',
          'backgroundColor': 'transparent',
        }}
      >
        <div className="box justify-between align-center">
          <div className="box">
            <div className="logo"></div>
            <Menu
              onClick={this.onChangeView}
              selectedKeys={[this.state.activeUrl]}
              mode="horizontal"
              className='nav'
              disabledOverflow={true}
              >
              {this.nav.map((item) => <Menu.Item key={item.url} className={item.url == this.state.activeUrl ? 'active-item' : 'item'}>
                <Link to={item.url}>{item.name}</Link>
              </Menu.Item>)}
            </Menu>
          </div>
        </div>
      </Header>
      <Content>
        <Routes>
          <Route path="/" element={<About />} />
          <Route path="/about" element={<About />} />
          <Route path="/list" element={<List />} />
          <Route path="/home" element={<Home />} />
        </Routes>
      </Content>
    </Layout>);
  }
}
 
export default App;
