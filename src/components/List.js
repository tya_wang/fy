// TODO: 表格字体颜色看不清
import React from 'react';
import { Button, Table, message } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import http from '../http/axios';
import '../style/List.css';

const columns = [{
    title: '实验名称',
    dataIndex: 'test_name',
    key: 'test_name',
  },
  {
    title: '日期',
    dataIndex: 'test_date',
    key: 'test_date',
  },
  {
    title: '左髋',
    dataIndex: 'lfemur',
    key: 'lfemur',
  },
  {
    title: '右髋',
    dataIndex: 'rfemur',
    key: 'rfemur',
  },
  {
    title: '左膝',
    dataIndex: 'ltibia',
    key: 'ltibia',
  },
  {
    title: '右膝',
    dataIndex: 'rtibia',
    key: 'rtibia',
  },
  {
    title: '左脚踝',
    dataIndex: 'lfoot',
    key: 'lfoot',
  },
  {
    title: '右脚踝',
    dataIndex: 'rfoot',
    key: 'rfoot',
  },
  {
    title: '颈部',
    dataIndex: 'head',
    key: 'head',
  },
  {
    title: '左肩',
    dataIndex: 'lradius',
    key: 'lradius',
  },
  {
    title: '左胸',
    dataIndex: 'lclavicle',
    key: 'lclavicle',
  },
  {
    title: '左胸',
    dataIndex: 'rclavicle',
    key: 'rclavicle',
  },
  {
    title: '右肩',
    dataIndex: 'rradius',
    key: 'rradius',
  },
  {
    title: '左肘',
    dataIndex: 'lradius',
    key: 'lradius',
  },
  {
    title: '右肘',
    dataIndex: 'rradius',
    key: 'rradius',
  },
  {
    title: '左手腕',
    dataIndex: 'lhand',
    key: 'lhand',
  },
  {
    title: '右手腕',
    dataIndex: 'rhand',
    key: 'rhand',
  }
];

function saveFile(url, filename) {
  let a = document.createElement("a");
  a.style = "display: none";
  a.href = url;
  a.download = filename;
  a.click();
  window.URL.revokeObjectURL(url);
  a.remove();
}

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      btnLoading: false,
      data: [],
      selectedRows: [],
    }
    this.downloadFile = this.downloadFile.bind(this);
  }

  componentDidMount() {
    const t = this;
    http.post('/return_video', {
      input_video_path: window.localStorage.getItem('fileName'),
    }).then((res) => {
      let data = res.data.chart_data;
      data.forEach((entry, index) => {
        entry.key = index;
      })
      t.setState({
        data: res.data.chart_data,
        loading: false,
      });
    })
  }

  downloadFile() {
    let requestedData = [];
    const t = this;
    if (this.state.selectedRows.length == 0) {
      message.error('请选择至少一条数据！');
      return
    }
    this.state.selectedRows.forEach((key) => {
      requestedData.push(this.state.data[key].video);
    })
    this.setState({ btnLoading: true });
    http.post('/output_frame', {
      input_video_list: requestedData,
      input_video_name: this.state.data[0].video,
    }).then(res => {
      t.setState({ btnLoading: false });
      saveFile(res.data.output_file_path, res.data.output_file_path.split("/")[3]);
    }).catch((err) => {
      t.setState({ btnLoading: false });
      message.error(`出现未知错误：${err}`);
    })
  }

  render() { 
    return (<div className='table'>
    <div className='box justify-between'>
      <Button
        type='primary'
        loading={this.state.btnLoading}
        onClick={this.downloadFile}
        icon={<DownloadOutlined />}
        style={{
          position: 'relative',
          left: '90%',
          marginBottom: '20px',
        }}>
        文件导出
      </Button>
    </div>
    <Table
      loading={this.state.loading}
      rowSelection={{
        selectedRowKeys: this.state.selectedRows,
        onChange: (selectedRows) => {
          this.setState({ selectedRows: selectedRows })
        },
      }}
      dataSource={this.state.data}
      columns={columns} />
  </div>);
  }
}
 
export default List;
