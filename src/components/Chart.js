import React from 'react';
import { Row, Col } from 'antd';
import * as echarts from 'echarts';
import ReactECharts from 'echarts-for-react';
import '../style/Chart.css';

const chartNames = [
  '髋关节',
  '膝关节',
  '脚踝',
  '颈部',
  '胸部',
  '肩部',
  '肘关节',
  '手腕',
]

const charts = [
  [{
    name: '左髋',
    value: 'lfemurArr'
  }, {
    name: '右髋',
    value: 'rfemurArr'
  }],
  [{
    name: '左膝',
    value: 'ltibiaArr'
  }, {
    name: '右膝',
    value: 'rtibiaArr'
  }],
  [{
    name: '左脚踝',
    value: 'lfootArr'
  }, {
    name: '右脚踝',
    value: 'rfootArr'
  }],
  [{
    name: '颈部',
    value: 'headArr'
  }],
  [{
    name: '左胸',
    value: 'lclavicleArr'
  }, {
    name: '右胸',
    value: 'rclavicleArr'
  }],
  [{
    name: '左肩',
    value: 'lhumerusArr'
  }, {
    name: '右肩',
    value: 'rhumerusArr'
  }],
  [{
    name: '左肘',
    value: 'lradiusArr'
  }, {
    name: '右肘',
    value: 'rradiusArr'
  }],
  [{
    name: '左手腕',
    value: 'lhandArr'
  }, {
    name: '右手腕',
    value: 'rhandArr'
  }],
]

function Charts(props) {

  let data = {
    lfemurArr: [], //左髋
    rfemurArr: [], //右髋
    ltibiaArr: [], //左膝
    rtibiaArr: [], //右膝
    lfootArr: [], //左脚踝
    rfootArr: [], //右脚踝
    headArr: [], //颈部
    lclavicleArr: [], //左胸
    rclavicleArr: [], //右胸
    lhumerusArr: [], //左肩
    rhumerusArr: [], //右肩
    lradiusArr: [], //左肘
    rradiusArr: [], //右肘
    lhandArr: [], //右手腕
    rhandArr: [], //右手腕
    date: [],//时间
    nameArr: []
  }


  props.data.forEach((i, index) => {
    data.lfemurArr.push(i.lfemur) //左髋
    data.rfemurArr.push(i.rfemur) //右髋
    data.ltibiaArr.push(i.ltibia) //左膝
    data.rtibiaArr.push(i.rtibia) //右膝
    data.lfootArr.push(i.lfoot) //左脚踝
    data.rfootArr.push(i.rfoot) //右脚踝
    data.headArr.push(i.head) //颈部
    data.lclavicleArr.push(i.lclavicle) //左胸
    data.rclavicleArr.push(i.rclavicle) //右胸
    data.lhumerusArr.push(i.lhumerus) //左肩
    data.rhumerusArr.push(i.rhumerus) //右肩
    data.lradiusArr.push(i.lradius) //左肘
    data.rradiusArr.push(i.rradius) //右肘
    data.lhandArr.push(i.lhand) //右手腕
    data.rhandArr.push(i.rhand) //右手腕
    data.date.push(index) //时间
  })

  let options = [];

  charts.forEach((i, index) => {
    let series = [];
    data.nameArr[index] = [];
    i.forEach((j, index2) => {
      data.nameArr[index].push(j.name);
      if (index2 == 0) {
        series.push({
          name: j.name,
          data: data[j.value],
          type: 'line',
          smooth: true,
        })
      } else if (index2 == 1) {
        series.push({
          name: j.name,
          data: data[j.value],
          type: 'line',
          smooth: true,
        })
      }
    })

    options[index] = {
      title: {
        left: '50px',
        text: chartNames[index],
        textStyle: {
          color: '8FE8F1',
        }
      },
      tooltip : {
        trigger: 'axis'
      },
      legend: {
        right: '50px',
        data: data.nameArr[index],
        textStyle: {
          color: ['#8EF2FB', '#31b5f3'],
        },
      },
      color: ['#8EF2FB', '#31b5f3'],
      grid: {
        left: '60',
        right: '50',
      },
      xAxis : [
        {
          type : 'category',
          boundaryGap : false,
          data : data.date
        }
      ],
      yAxis : [
        {
          type : 'value'
        }
      ],
      series: series
    };
  })

  return (
    <>
      <Row>
        {[...Array(3).keys()].map((i) => {
          return <Col span={8} key={i}><ReactECharts
            option={options[i]}
            className='chart'
          /></Col>
        })}
      </Row>
      <Row>
        {[...Array(3).keys()].map((i) => {
          return <Col span={8} key={i + 3}><ReactECharts
            option={options[i + 3]}
            className='chart'
          /></Col>
        })}
      </Row>
      <Row>
      {[...Array(2).keys()].map((i) => {
          return <Col span={8} key={i + 6}><ReactECharts
            option={options[i + 6]}
            className='chart'
          /></Col>
        })}
      </Row>
    </>
  )
}

function TrajactoryCharts(props) {
  return (
    <div className='traj-chart'>trajectory charts</div>
  )
}
 
export { Charts, TrajactoryCharts };
