import React from 'react';
import about from '../assets/about.jpg';

export default function About(props) {
  return (<div>
    <img src={about} style={{ 'margin': '60px 0'}} />
    <div style={{ 'color': 'white', 'fontSize': '16px' }}>
      <p>本系统旨在对冰雪运动视频中的运动员进行动作仿真与分析，</p>
      <p>应用场景包括辅助训练和风险预警。</p>
      <p>利用人工智能、深度学习技术，本系统能迅速、精确地给出人体运动仿真结果和身体力矩解算结果，并给出分析。</p>
      <p>本系统分为两个部分——仿真与分析。其中分析又分为力矩解算和异常预警两个部分。仿真部分接收冰雪运动视频输入，</p>
      <p>输出人体关键点位置与参数人体形状。分析部分以仿真结果作为输入，</p>
      <p>解算出每一个关节的力矩，并使用数据分析技术，矫正动作，并对潜在的风险进行预警。</p>
    </div>
  </div>)
}
