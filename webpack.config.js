const path = require('path');
const webpack = require('webpack');
const exec = require('child_process').exec;
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const mode = 'production';

let baseUrl;

if (mode == 'development') {
    baseUrl = 'http://127.0.0.1:5000/';
} else {
    baseUrl = 'http://10.41.0.113:5000/';
}

baseUrl = JSON.stringify(baseUrl);


module.exports = {
    mode: mode,
    entry: path.join(__dirname, 'src', 'index.js'),
    output: {
        path: path.join(__dirname, '..', 'static'),
        filename: '[name].[contenthash].js',
        clean: true,
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env', '@babel/preset-react'],
                }
            },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src', 'index.html'),
        }),
        new MiniCssExtractPlugin(),
        new webpack.DefinePlugin({
            apiRoot: baseUrl,
        }),
        {
            apply: (compiler) => {
                if (mode == 'development') {
                    compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {
                        exec('./post-build.sh', (err, stdout, stderr) => {
                            if (stdout) process.stdout.write(stdout);
                            if (stderr) process.stderr.write(stderr);
                        });
                    });
                }
            }
        },
    ],
    devtool: 'source-map',
}
